import React, { Component } from 'react';
import PropTypes from 'prop-types';

// import logo from './logo.svg';
// import './App.css';

// const App = () => <h1>Hello</h1>;
// stateles function:
// const App = () => <h1>HelloStateles</h1>

class App extends Component {
  render() {
    let txt2 = this.props.txt;
    let awesome = this.props.awesome;
    let mynumber = this.props.mynumber;
    return (
      <div>
        <h1 className="seriosly"> Hello Series.  </h1>
        <p>{this.props.txt}   <b>bold</b></p>
        <p>{awesome} <b>awesome</b></p>
        <p>{txt2}<b>2</b></p>
        <p><b>mynumber:</b> {mynumber}</p>
      </div>
    );
    // return React.createElement('h1', null, 'Hello Eggheads');
    // return <h1 className="seriosly"> Hello Series </h1>;
  }
}


//
// <div className="App">
//   <div className="App-header">
//     <img src={logo} className="App-logo" alt="logo" />
//     <h2>Welcome to React</h2>
//   </div>
//   <p className="App-intro">
//     To get started, edit <code>src/App.js</code> and save to reload.
//   </p>
// </div>

//   render() {
//     return (
//       <div className="App">
//         <div className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <h2>Welcome to React</h2>
//         </div>
//         <p className="App-intro">
//           To get started, edit <code>src/App.js</code> and save to reload.
//         </p>
//       </div>
//     );
//   }
// }
//
App.propTypes = {
  super: PropTypes.string,
  awesome: PropTypes.number,
  car: PropTypes.string.isRequired,
  txt: PropTypes.string.isRequired,
  mynumber: PropTypes.number.isRequired,
};

App.defaultProps = {
  txt: "This is te default text",
  mynumber: 9
};

export default App;
